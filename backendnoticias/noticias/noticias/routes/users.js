var express = require('express');
var router = express.Router();
let jwt = require('jsonwebtoken');
const personaC = require('../app/controls/PersonaControl');
let personaControl = new personaC();
const rolC = require('../app/controls/RolControl');
let rolControl = new rolC();
const noticiaC = require('../app/controls/NoticiaControl');
let noticiaControl = new noticiaC();
const cuentaC = require('../app/controls/CuentaControl');
let cuentaControl = new cuentaC();
const comentarioC = require('../app/controls/ComentarioControl');
let comentarioControl = new comentarioC();

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.send('que fue manito, estoy todo bien');
});

//middleware

const auth = function middleware(req, res, next) {
  const token = req.headers['news-token'];
  if (token === undefined) {
    res.status(401);
    res.json({ msg: "Error", tag: "falta token", code: 401 });
  } else {
    require('dotenv').config();
    const key = process.env.KEY_PRI;
    jwt.verify(token, key, async (err, decoded) => {
      if (err) {
        res.status(401);
        res.json({ msg: "Error", tag: "Token no valido  o expirado", code: 401 });
      } else {
        console.log(decoded.external)
        var models = require('../app/models');
        var cuenta = models.cuenta;
        const aux = cuenta.findOne({
          where: { external_id: decoded.external }
        });
        if (aux === null) {
          res.status(401);
          res.json({ msg: "Error", tag: "Token no valido", code: 401 });
        } else {
          //TODO AUTORIZACION
          next()
        }
      }
    });
  }
  //console.log(token);
  //console.log(next)
};

//inicio de sesion
router.post('/login', cuentaControl.inicio_sesion)

//api de personas
router.get('/admin/personas', auth, personaControl.listar);
router.post('/admin/usuarios/guardarUsuario', personaControl.guardar_Usuario);
router.post('/admin/personas/guardar', personaControl.guardar);
router.get('/admin/personas/buscar/:external', personaControl.obtener);
router.post('/admin/personas/modificar/:external', personaControl.modificar);

//api de rol
router.get('/admin/rol', rolControl.listar);
router.post('/admin/rol/guardar', rolControl.guardar);

//api de noticias
router.get('/noticias', noticiaControl.listar);
router.get('/noticias/estadoTrue', noticiaControl.listarNoticiasEnEstadoTrue);
router.get('/noticias/buscar/:external', noticiaControl.obtener);
router.post('/admin/noticias/guardar', noticiaControl.guardar);
router.post('/admin/noticias/guardar/archivo', noticiaControl.guardarFoto);

//api de comentarios
router.post('/admin/comentarios/guardar', comentarioControl.guardarComentario);
router.post('/comentarios/guardar', comentarioControl.crearComentario);
router.get('/comentarios/listarxnoticias/:idNoticia', comentarioControl.obtenerComentariosPorNoticia);
router.get('/comentarios/noticia/ubicaciones/:idNoticia', comentarioControl.obtenerComentariosparaUbicacionNoticia);
router.get('/comentarios/ubicacion', comentarioControl.obtenerTodosComentarios);
router.get('/perfil/:external', cuentaControl.obtenerPerfil);
router.put('/perfil/:external', cuentaControl.editarPerfil)
router.get('/micomentario/:id', comentarioControl.obtener);
router.get('/miscomentarios/:externalId', comentarioControl.misComentarios);
router.put('/editar/comentarios/:id', comentarioControl.editarComentarios);

router.get('/todosloscomentarios', comentarioControl.comentariodeTodos);
router.put('/banear/cuenta/:id', cuentaControl.banearCuenta);

module.exports = router;


//./node_modules/nodemon/bin/nodemon.js

//mapeo horizontal= cuando se mapea 3 relaciones fundamentales (uno a muchos, muchos a muchos, uno a uno)
//mapeo vertical = se puede realizar herencia
//herencia join table = simular herencias -- la llave primaria es llave primaria y foranea y no es incrementable
//single table = se crea una clase padre, absorve los datos de las clases hijos, se crea una columna de discriminador
//   = la tabla hija tiene sus datos y los datos de la clase padre
//no enviar id, enviar external id == 

/**
 * de uno a muchos --> id del fuerte al debil y arreglo del debil al fuerte
 * uno a uno --> tiene llave foranea unica
 * de uno a uno --> 
 */