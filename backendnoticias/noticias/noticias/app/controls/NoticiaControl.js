'use strict'
var models = require('../models');
var fs = require('fs');
const formidable = require('formidable');
const path = require('path');
var persona = models.persona;
var noticia = models.noticia;
//var extensiones = ['jpg', 'png', "mp4"]
var extensiones = ['jpg', 'png']

class NoticiaControl {

    async listar(req, res) {
        var lista = await noticia.findAll({
            attributes: ["titulo","external_id", "id", "cuerpo", "tipo_archivo", "archivo", "fecha", "tipo_noticia", "estado"],
            include: [
                { model: models.persona, as: "persona", attributes: ["apellidos", "nombres"] },
            ]
        });
        res.status(200);
        res.json({ msg: "OK", code: 200, datos: lista });
    }

    async listarNoticiasEnEstadoTrue(req, res) {
        var lista = await noticia.findAll({
            where: { estado: true },
            attributes: ["titulo","external_id", "id", "cuerpo", "tipo_archivo", "archivo", "fecha", "tipo_noticia", "estado"],
            include: [
                { model: models.persona, as: "persona", attributes: ["apellidos", "nombres"] },
            ]
        });
    
        // Modificar la lista para incluir la URL de la foto
        lista = await Promise.all(lista.map(async (item) => {
            const fotoAux = await noticia.findOne({ where: { external_id: item.external_id } });
            const fotoUrl = fotoAux ? path.join(__dirname, 'public', 'multimedia', fotoAux.archivo) : null;
    
            return {
                ...item.toJSON(),
                fotoUrl: fotoUrl
            };
        }));
    
        res.status(200);
        res.json({ msg: "OK", code: 200, datos: lista });
    }

    async obtener(req, res) {
        const external = req.params.external;
        var lista = await noticia.findAll({
            where: { external_id: external },
            attributes: ["titulo", ["external_id", "id"], "cuerpo", "fecha_nac", ["external_id", "id"], "tipo_archivo", "archivo", "fecha", "tipo_noticia", "estado"],
            include: [
                { model: models.persona, as: "persona", attributes: ["apellidos", "nombres"] },
            ]
        });
        if (lista === null || lista == undefined) {
            lista = {};
        }
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista });
    }

    async guardar(req, res) {

        if (req.body.hasOwnProperty('titulo') &&
            req.body.hasOwnProperty('cuerpo') &&
            req.body.hasOwnProperty('tipo_noticia') &&
            req.body.hasOwnProperty('persona') &&
            req.body.hasOwnProperty('fecha')) {
            var uuid = require('uuid');

            var per_aux = await persona.findOne({
                where: { external_id: req.body.persona},
                include: [
                    { model: models.rol, as: "rol", attributes: ["nombre"] }]
            });

            if (per_aux === undefined || per_aux === null) {
                res.status(401);
                res.json({ msg: "Error", tag: "No se encuentra el editor", code: 401 });
            } else {
                var data = {
                    titulo: req.body.titulo,
                    cuerpo: req.body.cuerpo,
                    fecha: req.body.fecha,
                    tipo_noticia: req.body.tipo_noticia,
                    id_persona: per_aux.id,
                    archivo: "noticia.png",
                    estado: false,
                    external_id: uuid.v4(),

                };

                if (per_aux.rol.nombre === "editor") {
                    var result = await noticia.create(data);

                    if (result === null) {
                        res.status(401);
                        res.json({ msg: "Error", tag: "No se pudo crear", code: 401 });
                    } else {
                        per_aux.external_id = uuid.v4();
                        await per_aux.save();
                        res.status(200);
                        res.json({ msg: "OK", code: 200 });
                    }

                } else {
                    res.status(400);
                    res.json({ msg: "Error", tag: "La persona que esta guardando la noticia no es un editor", code: 400 });
                }
            }
        } else {
            res.status(400);
            res.json({ msg: "Error", tag: "faltan datos", code: 400 });
        }

    }

    async guardarFoto(req, res) {
        var form = new formidable.IncomingForm(), files = [];

        form.on('file', function (field, file) {
            files.push(file);
        }).on('end', function () {
            console.log('OK');
        });
        form.parse(req, function (err, fields) {
            let listado = files;

            let external = fields.external[0];

            for (let index = 0; index < listado.length; index++) {
                var file = listado[index];

                //validacion de tamaño y tipo de archivo (hacer eso)

                var extension = file.originalFilename.split('.').pop().toLowerCase();

                if (extensiones.includes(extension)) {
                    if (file.size <= (2 * 1024 * 1024)) {
                        const name = external + '.' + extension;
                        console.log(extensiones);
                        fs.rename(file.filepath, "public/multimedia/" + name, async function () {
                            if (err) {
                                res.status(200);
                                res.json({ msg: "Error", tag: 'No se pudo guardar el archivo', code: 200 });
                            } else {
                                var notiAux = await noticia.findOne({ where: { external_id: external } });

                                notiAux.archivo = name;
                                notiAux.estado = true;
                                notiAux.save()

                                res.status(200);
                                res.json({ msg: "OK", tag: 'Archivo guardado', code: 200 });
                            }
                        });
                    } else {
                        res.status(400);
                        res.json({ msg: "ERROR", tag: 'solo se aceptan archivos de 2Mb', code: 400 });
                    }

                } else {
                    res.status(400);
                    res.json({ msg: "ERROR", tag: 'solo soporta ' + extensiones, code: 400 });
                }
            };
        });
    }
    

}

module.exports = NoticiaControl;