"use strict";

const { where } = require("sequelize");
var models = require("../models");
var persona = models.persona;
var rol = models.rol;
var cuenta = models.cuenta;
let jwt = require("jsonwebtoken");

class CuentaControl {
  async inicio_sesion(req, res) {
    if (req.body.hasOwnProperty("correo") && req.body.hasOwnProperty("clave")) {
      let cuentaA = await cuenta.findOne({
        where: { correo: req.body.correo },
        include: [
          {
            model: models.persona,
            as: "persona",
            attributes: ["apellidos", "nombres", "external_id"],
            include: [
              // Add this include block for the 'rol' model
              { model: models.rol, as: "rol", attributes: ["nombre"] },
            ],
          },
        ],
      });

      if (cuentaA === null) {
        res.status(400);
        res.json({ msg: "Error", tag: "Cuenta no existe", code: 400 });
      } else {
        if (cuentaA.estado === true) {
          if (cuentaA.clave === req.body.clave) {
            const token_data = {
              //se pueden enviar los permisos o rol.....
              external: cuentaA.external_id,
              check: true,
              rol: cuentaA.persona.rol.nombre,
            };

            //el tiempo depende de la aplicacion
            require("dotenv").config();
            const key = process.env.KEY_PRI;
            const token = jwt.sign(token_data, key, {
              expiresIn: "2h",
            });

            var info = {
              token: token,
              user: cuentaA.persona.apellidos + " " + cuentaA.persona.nombres,
              external: cuentaA.persona.external_id,
              rol: cuentaA.persona.rol.nombre,
            };
            res.status(200);
            res.json({ msg: "OK", tag: "Listo", datos: info, code: 200 });
          } else {
            res.status(400);
            res.json({
              msg: "Error",
              tag: "Correo o clave incorrectos",
              code: 400,
            });
          }
        } else {
          res.status(400);
          res.json({ msg: "Error", tag: "Cuenta desactivada", code: 400 });
        }
      }
    } else {
      res.status(400);
      res.json({ msg: "Error", tag: "faltan datos", code: 400 });
    }
  }


  async  banearCuenta(req, res) {
    try {
      const { id } = req.params;
  
      // Buscar la cuenta por su ID
      const cuentaAActualizar = await cuenta.findOne({
        where: { id_persona: id },
      });
  
      if (!cuentaAActualizar) {
        return res.status(404).json({
          code: 404,
          msg: "Cuenta no encontrada",
          tag: "Not Found",
          datos: {},
        });
      }
  
      // Actualizar el estado de la cuenta a false (banear)
      await cuentaAActualizar.update({ estado: false });
  
      return res.status(200).json({
        code: 200,
        msg: "Cuenta banneada correctamente",
        tag: "OK",
        datos: {},
      });
    } catch (error) {
      console.error(error);
      return res.status(500).json({
        code: 500,
        msg: "Error interno del servidor",
        tag: "Error",
        datos: {},
      });
    }
  }

  async obtenerPerfil(req, res) {
    try {
      const external_id = req.params.external;
      const personaExistente = await persona.findOne({
        where: { external_id },
        include: [{ model: cuenta, as: "cuenta" }],
      });

      return res.status(200).json({
        msg: "OK",
        code: 200,
        datos: {
          nombres: personaExistente.nombres,
          apellidos: personaExistente.apellidos,
          direccion: personaExistente.direccion,
          celular: personaExistente.celular,
          fecha_nac: personaExistente.fecha_nac,
          correo: personaExistente.cuenta.correo,
          clave: personaExistente.cuenta.clave,
          external_id: personaExistente.external_id,
        },
      });
    } catch (error) {
      console.error("Error al obtener el perfil:", error);
      throw new Error("Error al obtener el perfil");
    }
  }

  async editarPerfil(req, res) {
    try {
      // Obtener datos del cuerpo de la solicitud
      const {
        nombres,
        apellidos,
        direccion,
        celular,
        fecha_nac,
        correo,
        clave,
      } = req.body;

      // Obtener el perfil existente
      const external_id = req.params.external;
      const perfilExistente = await persona.findOne({
        where: { external_id },
        include: [{ model: cuenta, as: "cuenta" }],
      });

      if (!perfilExistente) {
        return res.status(404).json({ error: "Perfil no encontrado" });
      }

      // Actualizar los datos de la persona
      await perfilExistente.update({
        nombres: nombres || perfilExistente.nombres,
        apellidos: apellidos || perfilExistente.apellidos,
        direccion: direccion || perfilExistente.direccion,
        celular: celular || perfilExistente.celular,
        fecha_nac: fecha_nac || perfilExistente.fecha_nac,
      });

      // En el caso de tener información de cuenta, también puedes actualizarla

      // Actualizar la cuenta si existe
      await perfilExistente.cuenta.update({
        correo: correo || perfilExistente.cuenta.correo,
        clave: clave || perfilExistente.cuenta.clave, // Considera manejar la encriptación de la clave apropiadamente
      });

      return res.status(200).json({
        msg: "OK",
        code: 200,
        datos: {
          nombres: perfilExistente.nombres,
          apellidos: perfilExistente.apellidos,
          direccion: perfilExistente.direccion,
          celular: perfilExistente.celular,
          fecha_nac: perfilExistente.fecha_nac,
          correo: perfilExistente.cuenta.correo,
          external_id: perfilExistente.external_id,
        },
      });
    } catch (error) {
      console.error("Error al editar el perfil:", error);
      return res.status(500).json({ error: "Error interno del servidor" });
    }
  }
}
module.exports = CuentaControl;
