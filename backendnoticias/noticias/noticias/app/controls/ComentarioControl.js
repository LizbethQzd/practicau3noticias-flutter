"use strict";
const { Sequelize } = require('sequelize');

var models = require("../models");
var comentario = models.comentario;
var noticia = models.noticia;
var persona = models.persona;
var cuenta = models.cuenta;
class ComentarioControl {
  async listar(req, res) {
    var lista = await comentario.findAll({
      attributes: [
        "texto",
        "usuario",
        "longitud",
        "latitud",
        ["external_id", "id"],
      ],
      include: [
        { model: models.noticia, as: "noticia", attributes: ["titulo"] },
      ],
    });
    res.status(200);
    res.json({ msg: "OK", code: 200, datos: lista });
  }

  async obtener(req, res) {
    const id = req.params.id;
    const lista = await comentario.findByPk(id, {
      attributes: [
        "texto",
        "usuario",
        "longitud",
        "latitud",
        "external_id",
        "id",
      ],
    });

    if (lista === null) {
      lista = {};
    }
    res.status(200);
    res.json({ msg: "OK!", code: 200, datos: lista });
  }

  async guardarComentario(req, res) {
    if (
      req.body.hasOwnProperty("texto") &&
      req.body.hasOwnProperty("usuario") &&
      req.body.hasOwnProperty("longitud") &&
      req.body.hasOwnProperty("latitud") &&
      req.body.hasOwnProperty("noticia")
    ) {
      var uuid = require("uuid");
      var perAux = await persona.findOne({
        where: { external_id: req.body.usuario },
      });
      var noticiaAux = await noticia.findOne({
        where: { external_id: req.body.noticia },
      });
      if (noticiaAux != undefined) {
        var data = {
          texto: req.body.texto,
          usuario: perAux.id,
          longitud: req.body.longitud,
          latitud: req.body.latitud,
          id_noticia: noticiaAux.id,
          external_id: uuid.v4(),
        };
        let transaction = await models.sequelize.transaction();
        try {
          var result = await comentario.create(data, { transaction });
          await transaction.commit();
          if (result === null) {
            res.status(401);
            res.json({ msg: "ERROR", tag: "No se puede crear", code: 401 });
          } else {
            res.status(200);
            res.json({ msg: "OK", code: 200 });
          }
        } catch (error) {
          if (transaction) await transaction.rollback();
          res.status(203);
          res.json({ msg: "Error", code: 203, error_msg: error });
        }
      } else {
        res.status(400);
        res.json({
          msg: "ERROR",
          tag: "El dato a buscar no existe",
          code: 400,
        });
      }
    } else {
      res.status(400);
      res.json({ msg: "ERROR", tag: "Faltan datos", code: 400 });
    }
  }

  async crearComentario(req, res) {
    try {
      const { idNoticia, texto, usuario, latitud, longitud } = req.body;

      // Verificar si la noticia existe
      const noticiaExistente = await noticia.findByPk(idNoticia);
      if (!noticiaExistente) {
        return res.status(404).json({ error: "Noticia no encontrada" });
      }

      const nuevoComentario = await comentario.create({
        id_noticia: idNoticia,
        texto,
        usuario,
        latitud,
        longitud,
      });

      res.status(201).json(nuevoComentario);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Error interno del servidor" });
    }
  }

  async obtenerComentariosPorNoticia(req, res) {
    try {
      const idNoticia = req.params.idNoticia;
      const pagina = req.query.pagina || 1;
      const comentariosPorPagina = 10;

      const comentarios = await comentario.findAll({
        where: { id_noticia: idNoticia },
        offset: (pagina - 1) * comentariosPorPagina,
        limit: comentariosPorPagina,
        order: [["createdAt", "DESC"]],
      });

      if (comentarios.length === 0) {
        return res.status(404).json({
          code: 404,
          msg: "No se encontraron comentarios para la noticia",
          tag: "Not Found",
          datos: {},
        });
      }

      const externalIdsUsuarios = comentarios.map((com) => com.usuario);

      // Consultar las personas con esos external_ids de manera eficiente
      const personas = await persona.findAll({
        where: { external_id: externalIdsUsuarios },
        attributes: ["external_id", "nombres", "apellidos"],
      });

      // Crear un diccionario para mapear external_id de usuario a la persona
      const personasMap = personas.reduce((map, persona) => {
        map[persona.external_id] = persona;
        return map;
      }, {});

      // Asociar las personas a los comentarios
      comentarios.forEach((com) => {
        com.setDataValue("usuario", personasMap[com.usuario]);
      });

      return res.status(200).json({
        code: 200,
        msg: "Comentarios obtenidos correctamente",
        tag: "OK",
        datos: comentarios,
      });
    } catch (error) {
      console.error(error);
      return res.status(500).json({
        code: 500,
        msg: "Error interno del servidor",
        tag: "Error",
        datos: {},
      });
    }
  }

  async obtenerComentariosparaUbicacionNoticia(req, res) {
    try {
      const idNoticia = req.params.idNoticia;

      const comentarios = await comentario.findAll({
        where: { id_noticia: idNoticia },
        order: [["createdAt", "DESC"]],
        attributes: ["id", "latitud", "longitud", "usuario", "createdAt"],
      });

      if (comentarios.length === 0) {
        return res.status(404).json({
          code: 404,
          msg: "No se encontraron comentarios para la noticia",
          tag: "Not Found",
          datos: {},
        });
      }

      const externalIdsUsuarios = comentarios.map((com) => com.usuario);

      // Consultar las personas con esos external_ids de manera eficiente
      const personas = await persona.findAll({
        where: { external_id: externalIdsUsuarios },
        attributes: ["external_id", "nombres", "apellidos"],
      });

      // Crear un diccionario para mapear external_id de usuario a la persona
      const personasMap = personas.reduce((map, persona) => {
        map[persona.external_id] = persona;
        return map;
      }, {});

      // Asociar las personas a los comentarios
      const comentariosConUsuarios = comentarios.map((com) => {
        return {
          id: com.id,
          latitud: com.latitud,
          longitud: com.longitud,
          usuario: personasMap[com.usuario].nombres,
          createdAt: com.createdAt,
        };
      });

      return res.status(200).json({
        code: 200,
        msg: "Comentarios obtenidos correctamente",
        tag: "OK",
        datos: comentariosConUsuarios,
      });
    } catch (error) {
      console.error(error);
      return res.status(500).json({
        code: 500,
        msg: "Error interno del servidor",
        tag: "Error",
        datos: {},
      });
    }
  }

  async comentariodeTodos(req, res) {
    try {
        const comentarios = await comentario.findAll({
          order: [["createdAt", "DESC"]],
          attributes: ["id", "texto","latitud", "longitud", "usuario", "createdAt"],
        });
  
        if (comentarios.length === 0) {
          return res.status(404).json({
            code: 404,
            msg: "No se encontraron comentarios",
            tag: "Not Found",
            datos: {},
          });
        }
  
        const externalIdsUsuarios = comentarios.map((com) => com.usuario);
  
        // Consultar las personas con esos external_ids de manera eficiente
        const personas = await persona.findAll({
          where: { external_id: externalIdsUsuarios },
          attributes: ["external_id", "nombres", "apellidos", "id", "apellidos"],
        });
  
        // Crear un diccionario para mapear external_id de usuario a la persona
        const personasMap = personas.reduce((map, persona) => {
          map[persona.external_id] = persona;
          return map;
        }, {});
  
        // Asociar las personas a los comentarios
        const comentariosConUsuarios = comentarios.map((com) => {
          return {
            id: com.id,
            texto: com.texto,
            latitud: com.latitud,
            longitud: com.longitud,
            usuario: personasMap[com.usuario].nombres,
            idPersona: personasMap[com.usuario].id,
            createdAt: com.createdAt,
          };
        });
  
        return res.status(200).json({
          code: 200,
          msg: "Comentarios obtenidos correctamente",
          tag: "OK",
          datos: comentariosConUsuarios,
        });
      } catch (error) {
        console.error(error);
        return res.status(500).json({
          code: 500,
          msg: "Error interno del servidor",
          tag: "Error",
          datos: {},
        });
      }
    }
  
  

  async obtenerTodosComentarios(req, res) {
    try {
      const comentarios = await comentario.findAll({
        order: [["createdAt", "DESC"]],
        attributes: ["id", "latitud", "longitud", "usuario", "createdAt"],
      });

      if (comentarios.length === 0) {
        return res.status(404).json({
          code: 404,
          msg: "No se encontraron comentarios",
          tag: "Not Found",
          datos: {},
        });
      }

      const externalIdsUsuarios = comentarios.map((com) => com.usuario);

      // Consultar las personas con esos external_ids de manera eficiente
      const personas = await persona.findAll({
        where: { external_id: externalIdsUsuarios },
        attributes: ["external_id", "nombres", "apellidos"],
      });

      // Crear un diccionario para mapear external_id de usuario a la persona
      const personasMap = personas.reduce((map, persona) => {
        map[persona.external_id] = persona;
        return map;
      }, {});

      // Asociar las personas a los comentarios
      const comentariosConUsuarios = comentarios.map((com) => {
        return {
          id: com.id,
          latitud: com.latitud,
          longitud: com.longitud,
          usuario: personasMap[com.usuario].nombres,
          createdAt: com.createdAt,
        };
      });

      return res.status(200).json({
        code: 200,
        msg: "Comentarios obtenidos correctamente",
        tag: "OK",
        datos: comentariosConUsuarios,
      });
    } catch (error) {
      console.error(error);
      return res.status(500).json({
        code: 500,
        msg: "Error interno del servidor",
        tag: "Error",
        datos: {},
      });
    }
  }

  async misComentarios(req, res) {
    try {
      const externalId = req.params.externalId;

      const comentarios = await comentario.findAll({
        where: { usuario: externalId },
        order: [["createdAt", "DESC"]],
        attributes: [
          "id",
          "texto",
          "latitud",
          "longitud",
          "usuario",
          "createdAt",
        ],
      });

      if (comentarios.length === 0) {
        return res.status(404).json({
          code: 404,
          msg: "No se encontraron comentarios para el usuario",
          tag: "Not Found",
          datos: {},
        });
      }

      return res.status(200).json({
        code: 200,
        msg: "Comentarios obtenidos correctamente",
        tag: "OK",
        datos: comentarios,
      });
    } catch (error) {
      console.error(error);
      return res.status(500).json({
        code: 500,
        msg: "Error interno del servidor",
        tag: "Error",
        datos: {},
      });
    }
  }

  async editarComentarios(req, res) {
    const id = req.params.id;
    console.log(id);
    try {
      const { texto, usuario, latitud, longitud } = req.body;

      const comentarioExistente = await comentario.findByPk(id);
      if (!comentarioExistente) {
        return res.status(404).json({ error: "Comentario no encontrado" });
      }

      comentarioExistente.texto = texto;
      comentarioExistente.usuario = usuario;
      comentarioExistente.latitud = latitud;
      comentarioExistente.longitud = longitud;

      await comentarioExistente.save();

      res.status(200).json({
        code: 200,
        msg: "Comentario actualizado correctamente",
        tag: "OK",
        datos: comentarioExistente,
      });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Error interno del servidor" });
    }
  }
}

module.exports = ComentarioControl;
