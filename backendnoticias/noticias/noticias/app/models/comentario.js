'use strict'

module.exports = (sequelize, DataTypes) => {

    const comentario = sequelize.define('comentario', {
        texto: { type: DataTypes.STRING(150), defaultValue: "NONE" },
        usuario: { type: DataTypes.STRING(150), defaultValue: "NONE" },
        latitud: { type: DataTypes.DOUBLE, allowNull: true },
        longitud: { type: DataTypes.DOUBLE, allowNull: true },
        estado: { type: DataTypes.BOOLEAN, defaultValue: true },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 }

    }, { freezeTableName: true });
    comentario.associate = function (models) {
        comentario.belongsTo(models.noticia, { foreignKey: 'id_noticia' });
    }
    return comentario;
}