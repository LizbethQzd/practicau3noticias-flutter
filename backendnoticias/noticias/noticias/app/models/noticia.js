'use strict'

module.exports = (sequelize, DataTypes) => {

    const noticia = sequelize.define('noticia', {
        titulo: { type: DataTypes.STRING(150), defaultValue: "NONE" },
        cuerpo: { type: DataTypes.TEXT, defaultValue: "NONE" },
        tipo_noticia: { type: DataTypes.ENUM('NORMAL', 'DEPORTIVA', 'TECNOLOGICA', 'SOCIAL', 'URGENTE'), defaultValue: "NORMAL" },
        archivo: { type: DataTypes.STRING, defaultValue: "NONE" },
        tipo_archivo: { type: DataTypes.ENUM('VIDEO', 'IMAGEN'), defaultValue: "IMAGEN" },
        fecha: { type: DataTypes.DATEONLY },
        estado:{ type: DataTypes.BOOLEAN, defaultValue: true},
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 }

    }, { freezeTableName: true });
    noticia.associate = function (models) {
        noticia.belongsTo(models.persona, { foreignKey: 'id_persona' });
        noticia.hasMany(models.comentario, { foreignKey: 'id_noticia', as: 'comentario' });
    }
    return noticia;
}