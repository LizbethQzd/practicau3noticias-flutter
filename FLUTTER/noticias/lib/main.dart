import 'package:flutter/material.dart';
import 'package:noticias/views/comentarioView.dart';
import 'package:noticias/views/editarView.dart';
import 'package:noticias/views/revisarComentarios.dart';
import 'package:noticias/views/sessionView.dart';
import 'package:noticias/views/registerView.dart';
import 'package:noticias/views/exception/Page404View.dart';
import 'package:noticias/views/noticiasView.dart';
import 'package:noticias/views/editorView.dart';
import 'package:noticias/views/adminView.dart';
import 'package:noticias/views/adminP.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      //
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const SessionView(),
      initialRoute: '/home',
      routes: {
        '/home': (context) => const SessionView(),
        '/register': (context) => const RegisterView(),
        '/noticias': (context) => const NoticiasView(),
        '/editor': (context) => const EditorView(),
        '/admin': (context) => const AdminPView(),
        '/mapa': (context) => const AdminView(
              ubicaciones: [],
            ),
        '/editarperfil': (context) => EditarPerfilView(
              nombres: '',
              apellidos: '',
              direccion: '',
              celular: '',
              fechaNac: '',
              correo: '',
              clave: '',
              external_id: '',
            ),
        '/miscomentarios': (context) => MisComentariosView(
              externalId: "",
            ),
        '/revisioncomentarios': (context) => RevisionView()
      },
      onGenerateRoute: (settings) {
        return MaterialPageRoute(builder: (context) => const Page404View());
      },
    );
  }
}
