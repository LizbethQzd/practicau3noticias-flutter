import 'dart:convert';
import 'package:noticias/controls/Conexion.dart';
import 'package:noticias/controls/servicio.back/RespuestaGenerica.dart';
import 'package:noticias/controls/servicio.back/modelo/InicioSesionSW.dart';
import 'package:http/http.dart' as http;

class FacadeService {
  Conexion c = Conexion();

  Future<InicioSesionSW> inicioSesion(Map<String, String> mapa) async {
    Map<String, String> header = {'Content-Type': 'application/json'};

    final String url = '${c.URL}login';
    final uri = Uri.parse(url);

    InicioSesionSW isw = InicioSesionSW();

    try {
      final response =
          await http.post(uri, headers: header, body: jsonEncode(mapa));
      if (response.statusCode != 200) {
        if (response.statusCode == 404) {
          isw.code = 404;
          isw.msg = 'Error';
          isw.tag = 'Recurso no encontrado';
          isw.datos = {};
        } else {
          Map<dynamic, dynamic> mapa = jsonDecode(response.body);
          isw.code = mapa['code'];
          isw.msg = mapa['msg'];
          isw.tag = mapa['tag'];
          isw.datos = mapa['datos'];
        }
      } else {
        Map<dynamic, dynamic> mapa = jsonDecode(response.body);
        isw.code = mapa['code'];
        isw.msg = mapa['msg'];
        isw.tag = mapa['tag'];
        isw.datos = mapa['datos'];
      }
    } catch (e) {
      isw.code = 500;
      isw.msg = 'Error';
      isw.tag = 'Error inesperado';
      isw.datos = {};
    }
    return isw;
  }

  Future<RespuestaGenerica> listarAllNoticias() async {
    return await c.solicitudGet('noticias', false);
  }

  Future<InicioSesionSW> registrarUsuario(Map<String, String> mapa) async {
    Map<String, String> header = {'Content-Type': 'application/json'};

    final String url = '${c.URL}admin/usuarios/guardarUsuario';
    final uri = Uri.parse(url);

    InicioSesionSW isw = InicioSesionSW();

    try {
      final response =
          await http.post(uri, headers: header, body: jsonEncode(mapa));
      if (response.statusCode != 200) {
        if (response.statusCode == 404) {
          isw.code = 404;
          isw.msg = 'Error';
          isw.tag = 'Recurso no encontrado';
          isw.datos = {};
        } else {
          Map<dynamic, dynamic> mapa = jsonDecode(response.body);
          isw.code = mapa['code'];
          isw.msg = mapa['msg'];
          isw.tag = mapa['tag'];
          isw.datos = {};
        }
      } else {
        Map<dynamic, dynamic> mapa = jsonDecode(response.body);
        isw.code = mapa['code'];
        isw.msg = mapa['msg'];
        isw.tag = mapa['tag'];
        isw.datos = {};
      }
    } catch (e) {
      isw.code = 500;
      isw.msg = 'Error';
      isw.tag = 'Error inesperado';
      isw.datos = {};
    }
    return isw;
  }

  Future<InicioSesionSW> registrarComentario(Map<String, String> mapa) async {
    Map<String, String> header = {'Content-Type': 'application/json'};

    final String url = '${c.URL}admin/comentarios/guardarComentario';
    final uri = Uri.parse(url);

    InicioSesionSW isw = InicioSesionSW();

    try {
      final response =
          await http.post(uri, headers: header, body: jsonEncode(mapa));
      if (response.statusCode != 200) {
        if (response.statusCode == 404) {
          isw.code = 404;
          isw.msg = 'Error';
          isw.tag = 'Recurso no encontrado';
          isw.datos = {};
        } else {
          Map<dynamic, dynamic> mapa = jsonDecode(response.body);
          isw.code = mapa['code'];
          isw.msg = mapa['msg'];
          isw.tag = mapa['tag'];
          isw.datos = {};
        }
      } else {
        Map<dynamic, dynamic> mapa = jsonDecode(response.body);
        isw.code = mapa['code'];
        isw.msg = mapa['msg'];
        isw.tag = mapa['tag'];
        isw.datos = {};
      }
    } catch (e) {
      isw.code = 500;
      isw.msg = 'Error';
      isw.tag = 'Error inesperado';
      isw.datos = {};
    }
    return isw;
  }

  Future<RespuestaGenerica> crearComentario(Map<String, dynamic> data) async {
    Map<String, String> header = {'Content-Type': 'application/json'};

    final String url = '${c.URL}comentarios/guardar';
    final uri = Uri.parse(url);

    try {
      final response =
          await http.post(uri, headers: header, body: jsonEncode(data));
      if (response.statusCode != 201) {
        Map<dynamic, dynamic> mapa = jsonDecode(response.body);
        return RespuestaGenerica(
          code: mapa['code'],
          msg: mapa['msg'],
          datos: {},
        );
      } else {
        Map<dynamic, dynamic> mapa = jsonDecode(response.body);
        return RespuestaGenerica(
          code: mapa['code'],
          msg: mapa['msg'],
          datos: {},
        );
      }
    } catch (e) {
      return RespuestaGenerica(
        code: 500,
        msg: 'Error',
        datos: {},
      );
    }
  }

  Future<RespuestaGenerica> obtenerComentariosPorNoticia(
      int idNoticia, int pagina) async {
    return await c.solicitudGet(
        'comentarios/listarxnoticias/$idNoticia?pagina=$pagina', false);
  }

  Future<RespuestaGenerica> obtenerUbicacionNoticia(int idNoticia) async {
    return await c.solicitudGet(
        'comentarios/noticia/ubicaciones/$idNoticia', false);
  }

  Future<RespuestaGenerica> obtenerTodasUbicaciones() async {
    return await c.solicitudGet('comentarios/ubicacion', false);
  }

  Future<RespuestaGenerica> obtenerPerfil(String external) async {
    return await c.solicitudGet('perfil/$external', true);
  }

  Future<RespuestaGenerica> editarPerfil(
      Map<String, dynamic> datosUsuario) async {
    Map<String, String> header = {'Content-Type': 'application/json'};
    final String url = '${c.URL}perfil/${datosUsuario['external_id']}';
    final uri = Uri.parse(url);

    try {
      final response = await http.put(uri,
          headers: header,
          body: jsonEncode({
            'nombres': datosUsuario['nombres'],
            'apellidos': datosUsuario['apellidos'],
            'direccion': datosUsuario['direccion'],
            'celular': datosUsuario['celular'],
            'fecha_nac': datosUsuario['fecha_nac'],
            'correo': datosUsuario['correo'],
            'clave': datosUsuario['clave'],
            'external_id': datosUsuario['external_id'],
          }));

      if (response.statusCode != 200) {
        if (response.statusCode == 404) {
          return RespuestaGenerica(
            code: 404,
            msg: 'Recurso no encontrado',
            datos: {},
          );
        } else {
          Map<dynamic, dynamic> mapa = jsonDecode(response.body);
          return RespuestaGenerica(
            code: mapa['code'],
            msg: mapa['msg'],
            datos: {},
          );
        }
      } else {
        Map<dynamic, dynamic> mapa = jsonDecode(response.body);
        return RespuestaGenerica(
          code: mapa['code'],
          msg: mapa['msg'],
          datos: {},
        );
      }
    } catch (e) {
      return RespuestaGenerica(
        code: 500,
        msg: 'Error Inesperado',
        datos: {},
      );
    }
  }

  Future<RespuestaGenerica> obtenerComentario(int id) async {
    return await c.solicitudGet('micomentario/$id', false);
  }

  Future<RespuestaGenerica> miscomentarios(String externalId) async {
    return await c.solicitudGet('miscomentarios/$externalId', false);
  }

  Future<RespuestaGenerica> editarComentario(
      Map<String, dynamic> datosComentario) async {
    Map<String, String> header = {'Content-Type': 'application/json'};
    final String url = '${c.URL}editar/comentarios/${datosComentario['id']}';
    final uri = Uri.parse(url);

    try {
      final response = await http.put(uri,
          headers: header,
          body: jsonEncode({'texto': datosComentario['texto']}));

      if (response.statusCode != 200) {
        if (response.statusCode == 404) {
          return RespuestaGenerica(
            code: 404,
            msg: 'Recurso no encontrado',
            datos: {},
          );
        } else {
          Map<dynamic, dynamic> mapa = jsonDecode(response.body);
          return RespuestaGenerica(
            code: mapa['code'],
            msg: mapa['msg'],
            datos: {},
          );
        }
      } else {
        Map<dynamic, dynamic> mapa = jsonDecode(response.body);
        return RespuestaGenerica(
          code: mapa['code'],
          msg: mapa['msg'],
          datos: {},
        );
      }
    } catch (e) {
      return RespuestaGenerica(
        code: 500,
        msg: 'Error Inesperado',
        datos: {},
      );
    }
  }

  Future<RespuestaGenerica> obtenerTodosLosComentarios() async {
    return await c.solicitudGet('todosloscomentarios', false);
  }

  Future<RespuestaGenerica> banearCuenta(int id) async {
    Map<String, String> header = {'Content-Type': 'application/json'};
    final String url = '${c.URL}banear/cuenta/${id}';
    final uri = Uri.parse(url);

    try {
      final response = await http.put(uri, headers: header);
      //body: jsonEncode({'texto': datosComentario['texto']}));

      if (response.statusCode != 200) {
        if (response.statusCode == 404) {
          return RespuestaGenerica(
            code: 404,
            msg: 'Recurso no encontrado',
            datos: {},
          );
        } else {
          Map<dynamic, dynamic> mapa = jsonDecode(response.body);
          return RespuestaGenerica(
            code: mapa['code'],
            msg: mapa['msg'],
            datos: {},
          );
        }
      } else {
        Map<dynamic, dynamic> mapa = jsonDecode(response.body);
        return RespuestaGenerica(
          code: mapa['code'],
          msg: mapa['msg'],
          datos: {},
        );
      }
    } catch (e) {
      return RespuestaGenerica(
        code: 500,
        msg: 'Error Inesperado',
        datos: {},
      );
    }
  }
}
