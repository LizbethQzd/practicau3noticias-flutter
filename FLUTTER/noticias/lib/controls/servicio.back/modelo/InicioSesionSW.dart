import 'package:noticias/controls/servicio.back/RespuestaGenerica.dart';

class InicioSesionSW extends RespuestaGenerica {
  String tag = '';
  String rol = '';
  String external = '';
  InicioSesionSW({msg = '', code = 0, datos, this.tag = ''});
}
