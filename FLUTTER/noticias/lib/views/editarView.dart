import 'package:flutter/material.dart';
import 'package:noticias/controls/servicio.back/FacadeService.dart'; // Importa tu servicio de fachada

class EditarPerfilView extends StatefulWidget {
  final String nombres;
  final String apellidos;
  final String direccion;
  final String celular;
  final String fechaNac;
  final String correo;
  final String clave;
  final String external_id;

  EditarPerfilView(
      {required this.nombres,
      required this.apellidos,
      required this.direccion,
      required this.celular,
      required this.fechaNac,
      required this.correo,
      required this.clave,
      required this.external_id});

  @override
  _EditarPerfilViewState createState() => _EditarPerfilViewState();
}

class _EditarPerfilViewState extends State<EditarPerfilView> {
  TextEditingController nombresController = TextEditingController();
  TextEditingController apellidosController = TextEditingController();
  TextEditingController direccionController = TextEditingController();
  TextEditingController celularController = TextEditingController();
  TextEditingController fechaNacController = TextEditingController();
  TextEditingController correoController = TextEditingController();
  TextEditingController claveController = TextEditingController();

  @override
  void initState() {
    super.initState();
    // Inicializar los controladores con los datos proporcionados en el constructor
    nombresController.text = widget.nombres;
    apellidosController.text = widget.apellidos;
    direccionController.text = widget.direccion;
    celularController.text = widget.celular;
    fechaNacController.text = widget.fechaNac;
    correoController.text = widget.correo;
    claveController.text = widget.clave;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Editar Perfil'),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            TextField(
              controller: nombresController,
              decoration: InputDecoration(labelText: 'Nombres'),
            ),
            TextField(
              controller: apellidosController,
              decoration: InputDecoration(labelText: 'Apellidos'),
            ),
            TextField(
              controller: direccionController,
              decoration: InputDecoration(labelText: 'Dirección'),
            ),
            TextField(
              controller: celularController,
              decoration: InputDecoration(labelText: 'Celular'),
            ),
            TextField(
              controller: fechaNacController,
              decoration: InputDecoration(labelText: 'Fecha de Nacimiento'),
            ),
            TextField(
              controller: correoController,
              decoration: InputDecoration(labelText: 'Correo'),
            ),
            TextField(
              controller: claveController,
              decoration: InputDecoration(labelText: 'Clave'),
            ),
            // Otros campos de edición según sea necesario
            SizedBox(height: 16.0),
            ElevatedButton(
              onPressed: () {
                // Lógica para guardar los cambios en el perfil
                _guardarCambios();
              },
              child: Text('Guardar Cambios'),
            ),
          ],
        ),
      ),
    );
  }

  void _guardarCambios() async {
    // Obtener los valores actuales de los controladores
    String nuevosNombres = nombresController.text;
    String nuevosApellidos = apellidosController.text;
    String nuevaDireccion = direccionController.text;
    String nuevoCelular = celularController.text;
    String nuevaFechaNac = fechaNacController.text;
    String nuevoCorreo = correoController.text;
    String nuevaClave = claveController.text;

    // Crear un mapa con los nuevos datos del usuario
    Map<String, dynamic> nuevosDatosUsuario = {
      'nombres': nuevosNombres,
      'apellidos': nuevosApellidos,
      'direccion': nuevaDireccion,
      'celular': nuevoCelular,
      'fecha_nac': nuevaFechaNac,
      'correo': nuevoCorreo,
      'clave': nuevaClave,
      'external_id': widget.external_id,
    };

    try {
      // Enviar la solicitud para editar el perfil
      print('Enviando solicitud para editar el perfil...');
      print(nuevosDatosUsuario);
      final respuesta = await FacadeService().editarPerfil(nuevosDatosUsuario);

      if (respuesta.code == 200) {
        // Cambios guardados exitosamente
        // Puedes mostrar un mensaje al usuario
        print('Cambios guardados exitosamente');
        Navigator.pop(context); // Regresar a la pantalla anterior
      } else {
        // Manejar errores en la respuesta del servidor
        print('Error al guardar los cambios: ${respuesta.msg}');
      }
    } catch (error) {
      // Manejar errores de la solicitud
      print('Error inesperado al guardar los cambios: $error');
    }
  }
}
