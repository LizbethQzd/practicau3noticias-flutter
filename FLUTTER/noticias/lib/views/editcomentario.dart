import 'package:flutter/material.dart';
import 'package:noticias/controls/servicio.back/FacadeService.dart';
import 'package:noticias/controls/servicio.back/RespuestaGenerica.dart'; // Asegúrate de importar tu clase RespuestaGenerica

class EditarComentarioView extends StatefulWidget {
  final int idComentario;

  EditarComentarioView({required this.idComentario});

  @override
  _EditarComentarioViewState createState() => _EditarComentarioViewState();
}

class _EditarComentarioViewState extends State<EditarComentarioView> {
  TextEditingController comentarioController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _loadComentario();
  }

  Future<void> _loadComentario() async {
    try {
      final respuesta =
          await FacadeService().obtenerComentario(widget.idComentario);

      if (respuesta.code == 200) {
        // Actualizar el controlador con el texto del comentario existente
        setState(() {
          comentarioController.text = respuesta.datos['texto'] ?? '';
        });
      } else {
        print('Error al obtener el comentario: ${respuesta.msg}');
        // Puedes mostrar un mensaje al usuario indicando el error si es necesario
      }
    } catch (e) {
      print('Error inesperado al obtener el comentario: $e');
    }
  }

  Future<void> _editarComentario() async {
    try {
      print(comentarioController.text);
      final respuesta = await FacadeService().editarComentario({
        'id': widget.idComentario,
        'texto': comentarioController.text,
        // Otros campos que puedan ser necesarios actualizar
      });

      if (respuesta.code == 200) {
        // Comentario editado exitosamente
        // Puedes mostrar un mensaje al usuario
        print('Comentario editado exitosamente');
        Navigator.pop(context); // Regresar a la pantalla anterior
      } else {
        // Manejar errores en la respuesta del servidor
        print('Error al editar el comentario: ${respuesta.msg}');
      }
    } catch (error) {
      // Manejar errores de la solicitud
      print('Error inesperado al editar el comentario: $error');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Editar Comentario'),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            TextField(
              controller: comentarioController,
              decoration: InputDecoration(labelText: 'Comentario'),
            ),
            SizedBox(height: 16.0),
            ElevatedButton(
              onPressed: () {
                // Lógica para guardar los cambios en el comentario
                _editarComentario();
              },
              child: Text('Guardar Cambios'),
            ),
          ],
        ),
      ),
    );
  }
}
