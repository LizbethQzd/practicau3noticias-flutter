import 'package:flutter/material.dart';
import 'package:noticias/controls/servicio.back/FacadeService.dart';
import 'package:noticias/controls/utiles/Utiles.dart';
import 'package:geolocator/geolocator.dart';
import 'package:noticias/views/comentarioView.dart';
import 'package:noticias/views/editarView.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:app_settings/app_settings.dart';

class NoticiasView extends StatefulWidget {
  const NoticiasView({Key? key}) : super(key: key);

  @override
  _NoticiasViewState createState() => _NoticiasViewState();
}

class _NoticiasViewState extends State<NoticiasView> {
  List<dynamic> noticias = [];
  Map<int, List<dynamic>?> comentariosPorNoticia = {};
  Map<int, int> paginasComentarios = {};
  List<TextEditingController> _comentarioControllers = [];
  String externalIdUsuario = '';
  double? latitud;
  double? longitud;

  @override
  void initState() {
    super.initState();
    _getNoticias();
    _getExternalIdUsuario();
  }

  Future<void> _getNoticias() async {
    try {
      final response = await FacadeService().listarAllNoticias();

      if (response.code == 200) {
        setState(() {
          noticias = response.datos;
        });

        // Limpiar el controlador del comentario después de obtener las noticias
        _comentarioControllers.forEach((controller) => controller.clear());

        // Inicializar el controlador de páginas para cada noticia
        noticias.forEach((noticia) {
          paginasComentarios[noticia['id']] = 1;
        });

        // Obtener los comentarios para cada noticia
        for (var noticia in noticias) {
          await _getComentariosPorNoticia(noticia['id']);
        }
      } else {
        print('Error al obtener las noticias: ${response.msg}');
      }
    } catch (e) {
      print('Error inesperado: $e');
    }
  }

  Future<void> _getComentariosPorNoticia(int idNoticia) async {
    final pagina = paginasComentarios[idNoticia];
    final comentariosResponse = await FacadeService()
        .obtenerComentariosPorNoticia(idNoticia, pagina ?? 1);

    if (comentariosResponse.code == 200) {
      // Actualizar el estado para almacenar los comentarios
      setState(() {
        // Incrementar la página de comentarios
        paginasComentarios[idNoticia] = (pagina ?? 1) + 1;

        // Limpiar la lista de comentarios antes de agregar los nuevos
        comentariosPorNoticia[idNoticia] = comentariosResponse.datos;
      });
    } else {
      print('Error al obtener comentarios: ${comentariosResponse.msg}');
    }
  }

  Future<void> _getExternalIdUsuario() async {
    String? externalId = await Utiles().getValue('external');
    setState(() {
      externalIdUsuario = externalId ?? '';
    });
  }

  Future<void> _getLocation() async {
    try {
      Map<Permission, PermissionStatus> statuses = await [
        Permission.locationWhenInUse,
        Permission.locationAlways,
      ].request();

      if (statuses[Permission.locationWhenInUse] == PermissionStatus.granted ||
          statuses[Permission.locationAlways] == PermissionStatus.granted) {
        Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high,
        );
        setState(() {
          latitud = position.latitude;
          longitud = position.longitude;
        });
      } else {
        // Show a message to the user explaining the need for location permission
        // and provide an option to open app settings.
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content:
                Text('Location permission is required to access this feature.'),
            action: SnackBarAction(
              label: 'Open Settings',
              onPressed: () {
                openAppSettings(); // Open app settings if the user wants to grant permission.
              },
            ),
          ),
        );
      }
    } catch (e) {
      print('Error getting location: $e');
      // Handle the error, log it, or display a message to the user.
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Noticias'),
        actions: [
          IconButton(
            icon: Icon(Icons.edit),
            onPressed: () async {
              // Obtener los datos del perfil antes de navegar a la pantalla de edición
              try {
                final perfilResponse =
                    await FacadeService().obtenerPerfil(externalIdUsuario);

                if (perfilResponse.code == 200) {
                  Map<String, dynamic> datosPerfil = perfilResponse.datos;

                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => EditarPerfilView(
                        nombres: datosPerfil['nombres'] ?? '',
                        apellidos: datosPerfil['apellidos'] ?? '',
                        direccion: datosPerfil['direccion'] ?? '',
                        celular: datosPerfil['celular'] ?? '',
                        fechaNac: datosPerfil['fecha_nac'] ?? '',
                        correo: datosPerfil['correo'] ?? '',
                        clave: datosPerfil['clave'] ?? '',
                        external_id: datosPerfil['external_id'] ?? '',
                      ),
                    ),
                  );
                } else {
                  print('Error al obtener el perfil: ${perfilResponse.msg}');
                  // Puedes mostrar un mensaje al usuario indicando el error si es necesario
                }
              } catch (e) {
                print('Error inesperado al obtener el perfil: $e');
              }
            },
          ),
          IconButton(
            icon: Icon(Icons.comment), // Añadir el ícono deseado
            onPressed: () async {
              // Redireccionar a la vista de Mis Comentarios
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => MisComentariosView(
                    externalId: externalIdUsuario,
                  ),
                ),
              );
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: _buildNoticiasList(),
      ),
    );
  }

  Widget _buildNoticiasList() {
  if (noticias.isEmpty) {
    return const Center(child: Text('No hay noticias disponibles.'));
  }

  return ListView.builder(
    shrinkWrap: true,
    itemCount: noticias.length,
    itemBuilder: (context, index) {
      final noticia = noticias[index];
      _comentarioControllers.add(TextEditingController());

      final persona = noticia['persona'];
      final nombrePersona = '${persona['nombres']} ${persona['apellidos']}';

      return Card(
        margin: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ListTile(
              title: Text(
                noticia['titulo'],
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(noticia['id'].toString()),
                  if (noticia['archivo'] != null)
                    Center(
                      child: Image.network(
                        'http://192.168.1.108:3000/multimedia/' +
                            noticia['archivo'],
                        height: 100,
                        fit: BoxFit.cover,
                      ),
                    ),
                  Text('EDITOR: $nombrePersona'),
                  Text(noticia['fecha']),
                  Text(noticia['tipo_noticia']),
                  Text(
                    noticia['cuerpo'],
                    textAlign: TextAlign.justify,
                  ),
                  TextButton(
                    onPressed: () async {
                      // Ver comentarios y manejar paginación
                      await _showComentariosDialog(noticia['id']);
                    },
                    child: Text('Ver Comentarios'),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: _comentarioControllers[index],
                decoration: InputDecoration(
                  hintText: 'Añadir comentario...',
                  suffixIcon: IconButton(
                    icon: Icon(Icons.send),
                    onPressed: () async {
                      await _getLocation();
                      String textoComentario =
                          _comentarioControllers[index].text.trim();
                      String externalIdUsuarioActual = externalIdUsuario;

                      int externalIdNoticia = noticia['id'];

                      await FacadeService().crearComentario({
                        "idNoticia": externalIdNoticia,
                        "texto": textoComentario,
                        "usuario": externalIdUsuarioActual,
                        "latitud": latitud?.toString() ?? "0.1",
                        "longitud": longitud?.toString() ?? "0.1",
                      });
                      await _getComentariosPorNoticia(externalIdNoticia);
                      await _getNoticias();
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    },
  );
}


  Future<void> _showComentariosDialog(int idNoticia) async {
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (context, setState) {
            return AlertDialog(
              title: Text('Comentarios'),
              content: Container(
                width: double.maxFinite,
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: comentariosPorNoticia[idNoticia]?.length ?? 0,
                  itemBuilder: (context, index) {
                    var comentario = comentariosPorNoticia[idNoticia]?[index];
                    return ListTile(
                      title:
                          Text(comentario?['texto'] ?? 'Texto no disponible'),
                      subtitle: Text(
                        'Usuario: ${comentario?['usuario'] != null ? '${comentario['usuario']['nombres']} ${comentario['usuario']['apellidos']}' : 'Usuario no disponible'}',
                      ),
                    );
                  },
                ),
              ),
              actions: [
                ElevatedButton(
                  onPressed: () async {
                    // Manejar paginación para cargar más comentarios
                    await _getComentariosPorNoticia(idNoticia);
                    setState(() {});
                  },
                  child: Text('Cargar más comentarios'),
                ),
                ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                    paginasComentarios[idNoticia] = 1;
                  },
                  child: Text('Cerrar'),
                ),
              ],
            );
          },
        );
      },
    );
  }
}
