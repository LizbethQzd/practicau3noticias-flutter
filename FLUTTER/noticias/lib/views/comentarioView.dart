import 'package:flutter/material.dart';
import 'package:noticias/controls/servicio.back/FacadeService.dart';
import 'package:noticias/controls/servicio.back/RespuestaGenerica.dart';
import 'package:noticias/views/editcomentario.dart'; // Asegúrate de importar tu clase RespuestaGenerica

class MisComentariosView extends StatefulWidget {
  final String externalId;

  MisComentariosView({required this.externalId});

  @override
  _MisComentariosViewState createState() => _MisComentariosViewState();
}

class _MisComentariosViewState extends State<MisComentariosView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Mis Comentarios'),
      ),
      body: FutureBuilder<RespuestaGenerica>(
        future: FacadeService().miscomentarios(widget.externalId),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text('Error: ${snapshot.error}');
          } else if (snapshot.hasData && snapshot.data != null) {
            // Utilizar los datos obtenidos de snapshot.data
            return ListView.builder(
              itemCount: snapshot.data!.datos.length,
              itemBuilder: (context, index) {
                final comentario = snapshot.data!.datos[index];
                return ListTile(
                  title: Text(comentario['texto'] ?? ''),
                  subtitle: Text('Creado en: ${comentario['createdAt'] ?? ''}'),
                  trailing: IconButton(
                    icon: Icon(Icons.edit),
                    onPressed: () async {
                      // Navegar a la pantalla de edición al presionar el botón
                      await Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (context) => EditarComentarioView(
                            idComentario: comentario['id'],
                          ),
                        ),
                      );

                      // Después de editar, recargar los comentarios
                      setState(() {});
                    },
                  ),
                  // Puedes agregar más detalles según tus necesidades
                );
              },
            );
          } else {
            // Manejar el caso en que snapshot.data sea nulo
            return Text('No se obtuvieron datos');
          }
        },
      ),
    );
  }
}
