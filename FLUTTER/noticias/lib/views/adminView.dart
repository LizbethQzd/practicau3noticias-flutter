import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:noticias/controls/servicio.back/FacadeService.dart';

class AdminView extends StatefulWidget {
  final List<dynamic> ubicaciones;

  const AdminView({Key? key, required this.ubicaciones}) : super(key: key);

  @override
  _AdminViewState createState() => _AdminViewState();
}

class _AdminViewState extends State<AdminView> {
  bool _pageIsActive = true;

  @override
  void dispose() {
    _pageIsActive = false; // Indica que la página ya no está activa.
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<dynamic> ubicaciones = widget.ubicaciones;
    return Scaffold(
      body: Stack(
        children: [
          FlutterMap(
            options: MapOptions(
              center: LatLng(-4.0288124, -79.2071146),
              zoom: 13.2,
            ),
            children: [
              TileLayer(
                urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
                userAgentPackageName: 'com.example.app',
              ),
              MarkerLayer(
                markers: widget.ubicaciones.map((ubicacion) {
                  return buildMarker(
                    LatLng(ubicacion['latitud'], ubicacion['longitud']),
                    ubicacion['usuario'],
                  );
                }).toList(),
              )
            ],
          ),
          Positioned(
            top: 16.0,
            left: 16.0,
            child: FloatingActionButton(
              onPressed: () {
                _handleButtonPress();
              },
              child: Icon(Icons.arrow_back),
            ),
          ),
        ],
      ),
    );
  }

  void _handleButtonPress() {
    if (_pageIsActive) {
      Navigator.pop(
          context); // Realiza operaciones solo si la página aún está activa.
    }
  }

  TextStyle getDefaultTextStyle() {
    return const TextStyle(
      fontSize: 12,
      backgroundColor: Colors.black,
      color: Colors.white,
    );
  }

  Container buildTextWidget(String word) {
    return Container(
        alignment: Alignment.center,
        child: Text(word,
            textAlign: TextAlign.center, style: getDefaultTextStyle()));
  }

  Marker buildMarker(LatLng coordinates, String word) {
    return Marker(
        point: coordinates,
        width: 100,
        height: 12,
        child: buildTextWidget(word));
  }
}
