import 'package:flutter/material.dart';
import 'package:noticias/controls/servicio.back/FacadeService.dart';
import 'package:noticias/views/adminView.dart';
import 'package:noticias/views/revisarComentarios.dart';

class AdminPView extends StatefulWidget {
  const AdminPView({Key? key}) : super(key: key);

  @override
  _UsuarioViewState createState() => _UsuarioViewState();
}

class _UsuarioViewState extends State<AdminPView> {
  List<dynamic> noticias = [];
  List<dynamic> ubicaciones = [];

  @override
  void initState() {
    super.initState();
    _getNoticias();
    _getUbicaciones();
  }

  Future<void> _getNoticias() async {
    try {
      final response = await FacadeService().listarAllNoticias();

      if (response.code == 200) {
        setState(() {
          noticias = response.datos;
        });
      } else {
        print('Error al obtener las noticias: ${response.msg}');
      }
    } catch (e) {
      print('Error inesperado: $e');
    }
  }

  Future<void> _getUbicaciones() async {
    try {
      final response = await FacadeService().obtenerTodasUbicaciones();

      if (response.code == 200) {
        setState(() {
          ubicaciones = response.datos;
        });
      } else {
        print('Error al obtener las ubicaciones: ${response.msg}');
      }
    } catch (e) {
      print('Error inesperado: $e');
    }
  }

  void _mostrarUbicaciones(int idNoticia) async {
    final response = await FacadeService().obtenerUbicacionNoticia(idNoticia);

    if (response.code == 200) {
      List<dynamic> ubicaciones = response.datos;

      Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => AdminView(ubicaciones: ubicaciones),
      ));
    } else {
      print('Error al obtener las ubicaciones: ${response.msg}');
    }
  }

  void _mostrarTodasUbicaciones() {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => AdminView(ubicaciones: ubicaciones),
    ));
  }

  void _revisarComentarios() async {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => RevisionView(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Admin'),
        actions: [
          IconButton(
            onPressed: _revisarComentarios,
            icon: Icon(Icons.comment),
            tooltip: 'Revisar Comentarios',
          ),
        ],
      ),
      body: Column(
        children: [
          ElevatedButton(
            onPressed: _mostrarTodasUbicaciones,
            child: Text('Mostrar todos las ubicaciones'),
          ),
          Expanded(
            child: _buildNoticiasList(),
          ),
        ],
      ),
    );
  }

  Widget _buildNoticiasList() {
    if (noticias.isEmpty) {
      return const Center(child: Text('No hay noticias disponibles.'));
    }

    return ListView.builder(
      itemCount: noticias.length,
      itemBuilder: (context, index) {
        final noticia = noticias[index];

        // Use the card design function/widget here
        return buildNewsCard(noticia,
            'Nombre de la persona'); // Replace 'Nombre de la persona' with the actual name
      },
    );
  }

  Widget buildNewsCard(Map<String, dynamic> noticia, String nombrePersona) {
  return Card(
    margin: const EdgeInsets.all(8.0),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ListTile(
          title: Text(
            noticia['titulo'],
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(noticia['id'].toString()),
              if (noticia['archivo'] != null)
                Center(
                  child: Image.network(
                    'http://192.168.1.108:3000/multimedia/' + noticia['archivo'],
                    height: 100,
                    fit: BoxFit.cover,
                  ),
                ),
              Text('EDITOR: $nombrePersona'),
              Text(noticia['fecha']),
              Text(noticia['tipo_noticia']),
              Text(
                noticia['cuerpo'],
                textAlign: TextAlign.justify,
              ),
            ],
          ),
          onTap: () {
            _mostrarUbicaciones(noticia['id']);
          },
        ),
      ],
    ),
  );
}

}
