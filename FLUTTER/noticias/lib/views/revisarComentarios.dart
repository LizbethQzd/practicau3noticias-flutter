import 'package:flutter/material.dart';
import 'package:noticias/controls/servicio.back/FacadeService.dart';

class RevisionView extends StatefulWidget {
  @override
  _RevisionViewState createState() => _RevisionViewState();
}

class _RevisionViewState extends State<RevisionView> {
  List<dynamic> comentarios = [];

  @override
  void initState() {
    super.initState();
    _getComentarios();
  }

  Future<void> _getComentarios() async {
    try {
      final response = await FacadeService().obtenerTodosLosComentarios();

      if (response.code == 200) {
        setState(() {
          comentarios = response.datos;
        });
      } else {
        print('Error al obtener los comentarios: ${response.msg}');
      }
    } catch (e) {
      print('Error inesperado: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Revisar Comentarios'),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            for (var comentario in comentarios)
              ListTile(
                title: Text(comentario['texto']),
                subtitle: Text(
                    comentario['usuario'] + ' - ' + comentario['createdAt']),
                trailing: ElevatedButton(
                  onPressed: () {
                    _banearCuenta(comentario['idPersona']);
                  },
                  child: Text('Banear'),
                ),
              ),
          ],
        ),
      ),
    );
  }

  void _banearCuenta(int idPersona) async {
    try {
      final response = await FacadeService().banearCuenta(idPersona);

      if (response.code == 200) {
        print('Cuenta baneada exitosamente');
        // Actualizar la lista de comentarios después de banear la cuenta
        _getComentarios();
      } else {
        print('Error al banear la cuenta: ${response.msg}');
      }
    } catch (error) {
      print('Error inesperado al banear la cuenta: $error');
    }
  }
}
